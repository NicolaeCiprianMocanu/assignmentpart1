﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForms;

namespace AssignmentPart1
{
    class Rect : Shape
    {
        int width, height;
        Canvas canvass = new Canvas();
        Pen pen;
        SolidBrush brush;

        public Rect(Color color, int x, int y, int width, int height) : base(color, x, y)
        {
            this.width = width;
            this.height = height;
        }
        public override void draw(Graphics g)
        {
            
            if (canvass.fill == false)
            {
                g.DrawRectangle(canvass.Pen, canvass.xPos, canvass.yPos, width, height);
            }
            else
            {
                g.FillRectangle(canvass.myBrush, canvass.xPos, canvass.yPos, width, height);
            }
           
        }
    }
}
