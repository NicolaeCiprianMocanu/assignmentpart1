﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using WindowsForms;
using System.Collections;

namespace AssignmentPart1
{
    public partial class Form1 : Form
    {
        
        string user_command;
        public Boolean if_flag, while_flag, method_flag;
        string[] splitter;
      //  int i;
        const int screenWidth = 473;
        const int screenHeight = 345;
        Canvas myCanvas;
        string[] line;
        public int thickness,radius,width,height,point1,point2,point3;
        public int count;
       
        public int loopSize = 0;
        public int whileCommands = 0;
        string color, shape;
        
        int length;
        int compare_while;
        int compare_if;
        Bitmap OutputBitmap = new Bitmap(screenWidth, screenHeight);
        private int methodExecution;
        private Graphics g;

        public Form1()
        {
            InitializeComponent();
            myCanvas = new Canvas(Graphics.FromImage(OutputBitmap));
        }



        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);

        }


        private void commandLine_KeyDown(object sender, KeyEventArgs e)

        {///a listener is waiting for enter key to be pressed
            if (e.KeyCode == Keys.Enter)
            {

                // @param user_command gets what user typed into command line case insesitive,  
                // @method Trim is deleting whitespace found before the command when command line is cleared
                user_command = commandLine.Text.Trim().ToLower();
                /** @method Split is splitting user_command by any space founded inside off command
                 *
                 **/

                splitter = user_command.Split(' ');

                if (splitter[0].Equals("drawto"))
                {
                    if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command!Missing parameters");
                    }
                    else if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }

                    else
                    {

                        try
                        {
                            int X = Int32.Parse(splitter[1]);
                            int Y = Int32.Parse(splitter[2]);
                            myCanvas.drawTo(X, Y);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }


                    }
                }


                else if (splitter[0].Equals("pen"))

                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing pen color and thickness!");
                    }


                    else if (splitter[1].Equals("red"))
                    {

                        try
                        {

                            thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        myCanvas.pen(Color.Red, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Red);
                    }





                    else if (splitter[1].Equals("blue"))
                    {

                        try
                        {
                            thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        myCanvas.pen(Color.Blue, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Blue);



                    }

                    else if (splitter[1].Equals("yellow"))
                    {

                        try
                        {
                            int thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        myCanvas.pen(Color.Yellow, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Yellow);

                    }

                    else if (splitter[1].Equals("green"))
                    {
                        try
                        {
                            int thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        // when user change color of the pen, a new brush is created with same color

                        myCanvas.pen(Color.Green, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Green);

                    }

                    else
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an undefined color! Only blue,yellow,red,green colors are allowed");
                    }


                }
                else if (splitter[0].Equals("moveto"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }

                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Please enter 2 parameters");
                    }

                    else
                    {

                        try
                        {
                            int X = Int32.Parse(splitter[1]);
                            int Y = Int32.Parse(splitter[2]);
                            myCanvas.moveTo(X, Y);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter only numbers after moveto command!");
                        }


                    }
                }



                else if (splitter[0].Equals("rectangle"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing width and height parameters!");
                    }
                    else
                    {


                        try
                        {
                            int width = Int32.Parse(splitter[1]);
                            int height = Int32.Parse(splitter[2]);
                            myCanvas.Rectangle(width, height);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter 2 numbers after draw rectangle command");
                        }

                    }
                }
                else if (splitter[0].Equals("triangle"))
                {
                    if (splitter.Length > 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else if (splitter.Length < 4)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Please enter 3 parameters after draw triangle command!");
                    }
                    else
                    {


                        try
                        {
                            int side1 = Int32.Parse(splitter[1]);
                            int side2 = Int32.Parse(splitter[2]);
                            int side3 = Int32.Parse(splitter[3]);
                            myCanvas.Triangle(side1, side2, side3);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter side1, side2, and side3 values after draw triangle command!");
                        }
                    }

                }

                else if (splitter[0].Equals("circle"))
                {
                    if (splitter.Length > 2)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else if (splitter.Length < 2)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing radius!");
                    }
                    else
                    {

                        try
                        {
                            int radius = Int32.Parse(splitter[1]);
                            myCanvas.Circle(radius);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format! Please enter a number after draw circle command ");
                        }

                    }

                }




                else if (splitter[0].Equals("clear"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        myCanvas.clear();

                    }
                }
                else if (splitter[0].Equals("reset"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        //the canvas is cleared, the pen keep same position, color and thickness
                        myCanvas.reset();

                    }
                }

                else if (splitter[0].Equals("fillon"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        /**set the function fillon to true
                        //any shape drawn from now will pe filled by the brush
                        the brush will have the same color like the pen
                        always when user change the color of the pen, a new brush with
                        the same color is created and waits for the command fillon
                        all the shapes will be filled until fillof command
                        **/
                        myCanvas.fillOn();
                    }
                }
                else if (splitter[0].Equals("filloff"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        //the command disable the brush 
                        myCanvas.fillOff();
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an undefined command! Please try again!");
                }
                commandLine.Text = " ";
                Refresh();

            }
        }




        /**when a text is added into program area
         * @param program save all the text as a string without empty space and all lowercases
         * @param line is a string[] who splitts the text by new line
         * every single index of line represent a command to execute when run typed and enter pressed by the user
         * */
        private void programArea_TextChanged(object sender, EventArgs e)
        {
            string program = programArea.Text.Trim().ToLower();
            line = program.Split('\n');
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp;)|*.jpg; *.jpeg; *.gif; *.bmp;";
            save.Title = "Save Image File";

            if (save.ShowDialog() == DialogResult.OK)
            {
                string saveFile = save.FileName;


                try
                {
                    OutputBitmap.Save(saveFile);
                }
                /**when i am saving the picture, an exeption may occur 
                 * if the filename already exist an exeption is thrown
                 * picture not saved if exception ocurr
                 * */
                catch (System.Runtime.InteropServices.ExternalException w)
                {

                }

            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog load = new OpenFileDialog();
            //only picture format allowed
            load.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp;)|*.jpg; *.jpeg; *.gif; *.bmp;";
            if (load.ShowDialog() == DialogResult.OK)
            {
                string filename = load.FileName;
                canvas.Image = new Bitmap(load.FileName);
            }
        }

        private void Run_Click(object sender, EventArgs e)
        {
             length = line.Length;
            for (int i = 0; i < line.Length; i++)
            {
                try
                {
                    splitter = line[i].Split(' ');
                }
                catch(IndexOutOfRangeException p)
                {

                }
                if (splitter[0].Equals("drawto"))
                {
                    if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command!Missing parameters");
                    }
                    else if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }

                    else
                    {

                        try
                        {
                            int X = Int32.Parse(splitter[1]);
                            int Y = Int32.Parse(splitter[2]);
                            myCanvas.drawTo(X, Y);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }


                    }
                }


                else if (splitter[0].Equals("pen"))

                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing pen color and thickness!");
                    }


                    else if (splitter[1].Equals("red"))
                    {

                        try
                        {

                            thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        myCanvas.pen(Color.Red, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Red);
                    }





                    else if (splitter[1].Equals("blue"))
                    {

                        try
                        {
                            thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        myCanvas.pen(Color.Blue, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Blue);



                    }

                    else if (splitter[1].Equals("yellow"))
                    {

                        try
                        {
                            int thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        myCanvas.pen(Color.Yellow, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Yellow);

                    }

                    else if (splitter[1].Equals("green"))
                    {
                        try
                        {
                            int thickness = Int32.Parse(splitter[2]);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                        }
                        // when user change color of the pen, a new brush is created with same color

                        myCanvas.pen(Color.Green, thickness);
                        myCanvas.myBrush = new SolidBrush(Color.Green);

                    }

                    else
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an undefined color! Only blue,yellow,red,green colors are allowed");
                    }


                }
                else if (splitter[0].Equals("moveto"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }

                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Please enter 2 parameters");
                    }

                    else
                    {

                        try
                        {
                            int X = Int32.Parse(splitter[1]);
                            int Y = Int32.Parse(splitter[2]);
                            myCanvas.moveTo(X, Y);
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter only numbers after moveto command!");
                        }


                    }
                }



                else if (splitter[0].Equals("rectangle"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing width and height parameters!");
                    }
                    else
                    {
                        if ((splitter[1].Equals("width")) & (splitter[2].Equals("height")))
                        {
                            if (while_flag == true)
                            {/**
                              * if while condition is true
                              * increment the while body count
                              * execute the command
                              * continue to next command
                              * */
                                whileCommands++;
                                myCanvas.Rectangle(width, height);
                                continue;
                            }
                            if (while_flag == false)
                            {
                                /**
                                 * if while condition is false
                                 * ignore the command 
                                 * */
                                continue;
                            }

                            if (method_flag == false)
                            {
                                //width = Int32.Parse(splitter[1]);
                                //height = Int32.Parse(splitter[2]);
                                myCanvas.Rectangle(width, height);
                                continue;
                            }
                            if (method_flag == true)
                            {
                                continue;
                            }

                            if (if_flag == true)
                            {
                                /**
                              * when if  condition is true
                              *
                              * execute the command
                              * continue to next command
                              * */
                                myCanvas.Rectangle(width, height);
                                continue;
                            }
                            if (if_flag == false)
                            {
                                /**
                             * when if  condition is false
                             *ignore the command
                             
                             * */
                                continue;
                            }
                        }
                        else
                        {
                            if (if_flag == true)
                            {
                                try
                                {
                                    int width = Int32.Parse(splitter[1]);
                                    int height = Int32.Parse(splitter[2]);
                                    myCanvas.Rectangle(width, height);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter 2 numbers after draw rectangle command");
                                }
                            }
                            if (if_flag == false)
                            {
                                continue;
                            }
                            if (method_flag == false)
                            {
                                myCanvas.Rectangle(width, height);
                                continue;
                            }

                            if (method_flag == true)
                            {
                                continue;
                            }
                        }
                    }
                }
                /// <summary>cube is the extra shape added and is composed by 2 rectangles and 4 lines which connects the corners of the rectangles</summary>
                /// <param name="length">indicates the length of the lines which connects the rectangles</param>
                /// <param name="width">the width of the rectangles</param>
                /// /// <param name="height">the height of the rectangles</param>
                /// <returns>draw 2 rectangles and 4 lines to connect them</returns>
                else if (splitter[0].Equals("cube"))
                {
                    if (splitter.Length > 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line " + (i + 1));
                    }
                    else if (splitter.Length < 4)
                    {
                        System.Windows.Forms.MessageBox.Show(" Please enter 3 parameters after draw triangle command! You have entered an incomplete  command at line !" + (i + 1));
                    }
                    else
                    {
                        width = Int32.Parse(splitter[1]);
                        height = Int32.Parse(splitter[2]);
                        length = Int32.Parse(splitter[3]);
                        myCanvas.Rectangle(width, height);
                        myCanvas.moveTo(myCanvas.xPos, (myCanvas.yPos + length));
                        myCanvas.Rectangle(width, height);
                        myCanvas.drawTo((myCanvas.xPos - width), (myCanvas.yPos - length - height));
                        myCanvas.moveTo(myCanvas.xPos,( myCanvas.yPos-height));
                        myCanvas.drawTo((myCanvas.xPos + width), (myCanvas.yPos + length + height));
                        myCanvas.moveTo((myCanvas.xPos - width), myCanvas.yPos);
                        myCanvas.drawTo((myCanvas.xPos - width),( myCanvas.yPos-height-length));
                        myCanvas.moveTo(myCanvas.xPos,(myCanvas.yPos+height));
                        myCanvas.drawTo((myCanvas.xPos + width), (myCanvas.yPos + length + height));

                    }

                    }
               
                else if (splitter[0].Equals("triangle"))
                {
                    if (splitter.Length > 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line " + (i + 1));
                    }
                    else if (splitter.Length < 4)
                    {
                        System.Windows.Forms.MessageBox.Show(" Please enter 3 parameters after draw triangle command! You have entered an incomplete  command at line !" + (i + 1));
                    }
                    else
                    {
                        if ((splitter[1].Equals("point1")) & (splitter[2].Equals("point2")) & (splitter[3].Equals("point3")))
                        {
                            if (method_flag == false)
                            {
                                myCanvas.Triangle(point1, point2, point3);
                                continue;
                            }
                            if (method_flag == true)
                            {
                                continue;
                            }
                            if (if_flag == true)
                            {
                                /**
                              * when while or if condition is true
                              * increment the while body count
                              * execute the command
                              * continue to next command
                              * */
                                //whileCommands++;
                                myCanvas.Triangle(point1, point2, point3);
                                continue;
                            }
                            if (while_flag == true)
                            {
                                /**
                              * when while or if condition is true
                              * increment the while body count
                              * execute the command
                              * continue to next command
                              * */
                                whileCommands++;
                                myCanvas.Triangle(point1, point2, point3);
                                continue;
                            }
                            if (while_flag == false)
                            {
                                continue;
                            }

                                if (if_flag == false)
                                {
                                    continue;
                                }
                              
                            
                           
                        }
                        else
                        {
                            if (if_flag == true)
                            {
                                try
                                {
                                    int side1 = Int32.Parse(splitter[1]);
                                    int side2 = Int32.Parse(splitter[2]);
                                    int side3 = Int32.Parse(splitter[3]);
                                    myCanvas.Triangle(side1, side2, side3);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format at line !" + (i + 1));
                                }
                            }
                            else
                            {
                                continue;
                            }
                            if (method_flag == false)
                            {
                                try
                                {
                                    int side1 = Int32.Parse(splitter[1]);
                                    int side2 = Int32.Parse(splitter[2]);
                                    int side3 = Int32.Parse(splitter[3]);
                                    myCanvas.Triangle(side1, side2, side3);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format at line !" + (i + 1));
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }

                else if (splitter[0].Equals("circle"))
                {
                    /// <summary>circle command length is 2 </summary>
                    /// <param name="splitter.Length">the length of the splitter when circle is the first index</param>
                   
                    /// <returns>throws an excption when length different than 2</returns>
                    if (splitter.Length > 2)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command at line " + (i + 1));
                    }
                    else if (splitter.Length < 2)
                    {
                        System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command at line " + (i + 1));
                    }



                    else
                    {
                        if (splitter[1].Equals("radius"))
                        {
                            /// <summary>when while condition is true, whileCommands increments for each command in the while loop</summary>
                            /// <param name="while_flag">the flag for the loop condition</param>
                            /// <param name="whileCommands">while commands length counter</param>
                            /// <returns>draw a circle and continue with execution</returns>
                            if (while_flag = true)
                            {
                                whileCommands++;
                               
                                myCanvas.Circle(radius);
                                continue;
                            }
                            /// <summary>when while condition is false , ingnore commands</summary>
                            /// <returns>ignore commands</returns>

                             if (while_flag = false)
                            {
                                continue;
                            }
                            /// <summary>when method is called method flag set to false, execution is on</summary>
                            /// <param name="method_flag">indicates if execution is on or off when are inside method</param>
                            /// <param name="radius">the radius of the circle</param>
                            /// <returns>draw a circle and continue with execution</returns>
                            if (method_flag = false)
                            {
                               
                                myCanvas.Circle(radius);
                                continue;
                            }
                            /// <summary>when are inside a method declaration, ingnore commands</summary>
                            /// /// <param name="method_flag">indicates if execution is on or off when are inside method</param>
                            /// <returns>ignore commands</returns>
                            if (method_flag = true)
                            {
                                continue;
                            }
                            /// <summary>when if condition is true if_flag set to true, execution is on</summary>
                            /// <param name="if_flag">indicates if execution is on or off when are inside if statement</param>
                            /// <param name="radius">the radius of the circle</param>
                            /// <returns>draw a circle and continue with execution</returns>
                            if (if_flag = true)
                            {

                                myCanvas.Circle(radius);

                                continue;
                            }

                            /// <summary>when if condition is false, ingnore commands</summary>
                            /// /// <param name="if_flag">indicates that execution is off</param>
                            /// <returns>ignore commands</returns>
                            if (if_flag = false)
                            {
                                continue;
                            }

                            
                        }
                        /// <summary>count is the variable defined for loop operations</summary>
                        /// <param name="count">indicates the variable name</param>
                       /// <returns>draw a circle and continue with execution</returns>
                        else if (splitter[1].Equals("count"))
                        {
                            if (if_flag = true)
                            {
                                
                                    myCanvas.Circle(count);
                                /// <summary>checking if circle count is the last command from the program</summary>
                                /// <param name="line.Length">total number of commands in the program</param>
                                /// <param name="i">i represent the command number </param>
                                /// <returns>reset the program area if is the last command or continue if not</returns>
                                if (i != (line.Length-1))
                                {
                                    continue;
                                }
                                else
                                {
                                    
                                }

                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            if (if_flag == true)
                            {
                                radius = Int32.Parse(splitter[1]);
                                myCanvas.Circle(radius);
                                if (i != (line.Length - 1))
                                {
                                    continue;
                                }
                                else
                                {
                                    //break;
                                }
                            }
                            if (if_flag == false)
                            {
                                if (i != (line.Length - 1))
                                {
                                    continue;
                                }
                                else
                                {
                                    //break;
                                }
                            }



                            /// <summary>when the method is called all the commands inside method are executed</summary>
                            /// <param name="radius">the radius of the circle</param>
                            /// <param name="splitter[1]">the second index of the splitter</param>
                            /// <returns>draw a circle</returns>
                            
                            if (method_flag == false)
                            {
                                radius = Int32.Parse(splitter[1]);
                                myCanvas.Circle(radius);
                                continue;
                            }
                            /// <summary>when are inside a method definition, ignore commands inside method</summary>
                            /// <returns>ignore the commands</returns>
                            if (method_flag == true)
                            {
                                continue;
                            }
                        }
                    }
                }


                ///


                else if (splitter[0].Equals("endif"))
                {
                    if (splitter.Length == 1)
                    {
                        whileCommands++;
                        /**
                         * the end of if body
                         * finish execution
                         * **/
                        if (i != (line.Length - 1))
                        {
                            continue;
                        }
                        else
                        {
                            //break;
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("wrong command at line " + (i + 1));
                    }
                }

                else if (splitter[0].Equals("endwhile"))
                {
                    if (splitter.Length == 1)
                    {
                        if (while_flag = true)
                        {
                           
                            // param whileCommands counts the length of the while
                            whileCommands++;

                            ///if while flag is true i am returning back where is the starting of the while statement and do it  again if is true 
                            //@param whileCommands is the counter for while body 
                            i -= whileCommands;
                            whileCommands = 0;
                            continue;
                           
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("wrong command at line " + (i + 1));
                    }

                }



                else if (splitter[0].Equals("clear"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        myCanvas.clear();

                    }
                }
                else if (splitter[0].Equals("reset"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        //the canvas is cleared, the pen keep same position, color and thickness
                        myCanvas.reset();

                    }
                }

                else if (splitter[0].Equals("fillon"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        /**set the function fillon to true
                        //any shape drawn from now will pe filled by the brush
                        the brush will have the same color like the pen
                        always when user change the color of the pen, a new brush with
                        the same color is created and waits for the command fillon
                        all the shapes will be filled until fillof command
                        **/
                                myCanvas.fillOn();
                    }
                }
                else if (splitter[0].Equals("filloff"))
                {
                    if (splitter.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                    }
                    else
                    {
                        //the command disable the brush 
                        myCanvas.fillOff();
                    }
                }
                else if (splitter[0].Equals("radius"))
                {

                    if (splitter[1].Equals("="))
                    {
                        /**
                         * variable instantiation when = sign used
                         * */
                        if (splitter.Length > 3)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                        }
                        else if (splitter.Length < 3)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                        }
                        else
                        {
                            try
                            {
                                radius = Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }
                    }

                    else if (splitter[1].Equals("+="))
                    {
                        /**
                         * variable increment when += sign used
                         * */
                        if (splitter.Length > 3)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                        }
                        else if (splitter.Length < 3)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                        }
                        else
                        {
                            if (while_flag = true)
                            {
                                try
                                {
                                    whileCommands++;
                                    radius = radius + Int32.Parse(splitter[2]);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                    }

                }

                else if (splitter[0].Equals("width"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {

                        if (splitter[1].Equals("="))
                        {
                            /**
                             * width variable instantiation
                             **/
                            try
                            {
                                width = Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }

                        }

                        else if (splitter[1].Equals("+="))
                        {
                            /**
                             * width variable increment
                             **/
                            if (while_flag = true)
                            {
                                try
                                {
                                    whileCommands++;
                                    width = width + Int32.Parse(splitter[2]);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                        }
                    }
                }

                else if (splitter[0].Equals("height"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {

                        if (splitter[1].Equals("="))
                        {
                            /**
                             * variable height instantiation
                             * */

                            try
                            {
                                height = Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }

                        else if (splitter[1].Equals("+="))
                        {
                            /**
                            * variable height increment when we are inside loop body
                            * */
                            if (while_flag = true)
                            {
                                try
                                {
                                    /**
                                     *@param whileCommand is incremented because we are in the loop body
                                     *@param height is incremented by the third value of @param splitter
                                     *continue to next command
                                     * */
                                    whileCommands++;
                                    height = height + Int32.Parse(splitter[2]);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                                }
                            }
                            else
                            {
                                /**
                                 * ignore the command if while flag is set to false
                                 * */
                                continue;
                            }
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                        }
                    }
                }

                else if (splitter[0].Equals("count"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {
                        if (splitter[1].Equals("="))
                        {
                            /**
                             *  @param count instantiation before loop
                             * */
                            try
                            {
                                count = Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }
                        else if (splitter[1].Equals("+="))
                        {
                            if (while_flag = true)
                            {
                                try
                                {
                                    /**
                                     * whe are inside loop when is set to true
                                     * @param whileCommands increment by 1 because it counts the loop body
                                     * @param count increment by the 3rd value of @param splitter
                                     * continue to next command 
                                     * */
                                    whileCommands++;
                                    count = count + Int32.Parse(splitter[2]);
                                    continue;
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                                }
                            }
                            else
                            {
                                /**
                                 * when while flag is set to false
                                 * ignore command
                                 * */
                                continue;
                            }

                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                        }
                    }
                }

                else if (splitter[0].Equals("point1"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {
                        if (splitter[1].Equals("="))
                        {
                            try
                            {
                                /**
                                 * @param point1 instantiation before loop
                                 * */
                               
                                point1 = Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }
                        else if (splitter[1].Equals("+="))
                        {
                            try
                            {
                                whileCommands++;
                                point1 += Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }
                    }
                }
                else if (splitter[0].Equals("point2"))
                {
                    if (splitter.Length > 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 3)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {
                        if (splitter[1].Equals("="))
                        {
                            /**
                                 * @param point2 instantiation before loop
                                 * */
                            try
                            {
                                point2 = Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }
                        else if (splitter[1].Equals("+="))
                        {
                            try
                            {
                                whileCommands++;
                                point2 += Int32.Parse(splitter[2]);
                                continue;
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                            }
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                        }
                    }
                }
                else if (splitter[0].Equals("point3"))
                {

                    if (splitter[1].Equals("="))
                    {
                        try
                        {
                            /**
                                 * @param point3 instantiation before loop
                                 * */
                            point3 = Int32.Parse(splitter[2]);
                            continue;
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                        }
                    }
                    else if (splitter[1].Equals("+="))
                    {
                        try
                        {
                            whileCommands++;
                            point3 += Int32.Parse(splitter[2]);
                            continue;
                        }
                        catch (System.FormatException f)
                        {
                            System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                    }
                }


                else if (splitter[0].Equals("if"))
                {
                    if (splitter.Length > 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {

                        if (splitter[1].Equals("count"))
                        {
                            if (splitter[2].Equals("="))
                            {
                                whileCommands++;
                                try
                                {
                                    /**
                                     * @param compare_if is the expected value of @param count
                                     * on the if command
                                     * */
                                    compare_if = Int32.Parse(splitter[3]);
                                }
                                catch (System.FormatException f)
                                {
                                    System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format at line " + (i + 1));
                                }
                                if (compare_if == count)
                                {
                                    /**
                                     * if @param compare_if and @param count are equal
                                     * if flag set to true
                                     * command inside if will be executed
                                     * */
                                    if_flag = true;
                                    continue;
                                }
                                else
                                {
                                    /**
                                     * if @param compare_if and @param count are not equal
                                     * if flag set to false
                                     * command inside if will not be executed
                                     * */
                                    if_flag = false;
                                    continue;
                                }
                            }
                            else
                            {
                                System.Windows.Forms.MessageBox.Show("wrong sign used at line " + (i + 1));
                            }
                        }
                    }
                }
                else if (splitter[0].Equals("while"))
                {
                    if (splitter.Length > 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                    }
                    else if (splitter.Length < 4)
                    {
                        System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                    }
                    else
                    {

                        if ((splitter[1].Equals("count")) & (splitter[2].Equals("<")))
                        {
                            try
                            {
                                /**
                              * @param compare_while store the last index of @param splitter
                              * 
                              * */
                                compare_while = Int32.Parse(splitter[3]);
                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("you entered a wrong parameter for while at line " + (i + 1));
                            }
                            if (count < compare_while)
                            {/**
                              *if the condition in the while is true 
                              *@param count is less than @param compare_while
                              *while flag set to true
                              *increment the while body counter
                              *continue to next command
                              * */
                                while_flag = true;
                                whileCommands++;
                                continue;
                            }
                            else
                            {
                                /**
                                 * if the condition in the while is false
                                 * while_flag set to false
                                 * ignore commands
                                 * */
                                while_flag = false;
                            }

                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("wrong sign or parameter used at line " + (i + 1));
                        }
                    }
                }
                
                else if (splitter[0].Equals("method"))
                {

                    if (splitter[1].Equals("noparam"))
                    {
                        

                        if (splitter.Length > 2)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an invalid method command!To many parameters at line" + (i + 1));
                        }
                        else if (splitter.Length < 2)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an incomplete method command at line" + (i + 1));
                        }
                        /// <summary>this is a method definition when execution is off</summary>
                        /// <param name="noparam">a method without parameter</param>
                        /// <param name="method_flag">the flag is on so the execution is off</param>
                        else
                        {
                            method_flag = true;
                            continue;
                        }
                    }
                    else if (splitter[1].Equals("param"))
                    {
                        

                        if (splitter.Length > 3)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters at line" + (i + 1));
                        }
                        else if (splitter.Length < 3)
                        {
                            System.Windows.Forms.MessageBox.Show("you have entered an incomplete command at line" + (i + 1));
                        }
                        else
                        {
                            method_flag = true;
                            continue;
                        }
                    }
                    else
                    {

                        System.Windows.Forms.MessageBox.Show("you entered a wrong method name at line " + (i + 1));
                    }
                }

                else if (splitter[0].Equals("endmethod"))
                {

                    if (splitter.Length == 1)
                    {
                        
                        if (i != (line.Length - 1))
                        {
                            continue;
                        }
                        else
                        {
                            //break;
                        }
                        
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("you entered a wrong command at line " + (i + 1));
                    }

                }

                else if (splitter[0].Equals("noparam"))
                {
                    if (splitter.Length == 1)
                    {
                        if (methodExecution < 1)
                        {
                            method_flag = false;
                            methodExecution++;
                            i -= 3;
                            continue;
                        }
                        else
                        {
                            methodExecution = 0;
                            method_flag = true;

                        }



                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("you entered a wrong command at line " + (i + 1));
                    }

                }

                else if (splitter[0].Equals("param"))
                {//when the method name is called
                    //i save the variable value
                    //comeback 2 lines back where is the command
                    //reset the flag to false so the command can be executed now
                    if (splitter.Length == 2)
                    {
                        if (methodExecution < 1)
                        {
                            try
                            {

                                methodExecution++;
                                radius = Int32.Parse(splitter[1]);
                                method_flag = false;
                                i -= 3;
                                continue;

                            }
                            catch (System.FormatException f)
                            {
                                System.Windows.Forms.MessageBox.Show("you entered a wrong parameter for method at line " + (i + 1));
                            }
                        }
                        else
                        {
                            methodExecution = 0;
                            method_flag = true;

                        }
                        
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("you entered a wrong command at line " + (i + 1));
                    }

                }

                else
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an undefined command! Please try again! LINE " + (i + 1));
                }
                programArea.Text = " ";
                Refresh();
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }
    }
}
