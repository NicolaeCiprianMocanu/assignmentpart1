﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForms;

namespace AssignmentPart1
{
    class Triangle : Shape
    {
        int side1, side2, side3;
        private static Canvas canvas = new Canvas();
        public Triangle(Color color, int x, int y, int side1, int side2, int side3) : base(color, x, y)
        {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
        }
       
        public override void draw(Graphics g)
        {
            Point AB = new Point((x + side1), y);
            Point AC = new Point(x, (y + side2));
            Point BC = new Point(x, y);
            Point[] point = new Point[] { AB, AC, BC };
            if (canvas.fill == false)
            {
                g.DrawPolygon(canvas.Pen, point);
            }
            else
            {
                g.FillPolygon(canvas.myBrush, point);
            }
            
        }
    }
}
