﻿using AssignmentPart1;
using System;
using System.ComponentModel;
using System.Drawing;


namespace WindowsForms
{
    public class Canvas
    {


        public int xPos, yPos;
        public Color color1;
        int thickness;
        public SolidBrush myBrush;
        public Pen Pen;
        string[] splitter;
        string color;
        string shape;
        public Boolean fill = false;
        public Graphics g;

        
        public Canvas(Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
            myBrush = new SolidBrush(Color.Black);

        }

      

        public Canvas()
        {

           // xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
            myBrush = new SolidBrush(Color.Black);

        }


        public void drawTo(int toX, int toY)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY;

        }

        public void pen(Color penColor, int penThickness)
        {
            this.color1 = penColor;
            this.thickness = penThickness;
            Pen = new Pen(penColor, penThickness);
        }

        public void moveTo(int toX, int toY)
        {
            xPos = toX;
            yPos = toY;
        }

        public void Rectangle(int width, int height)
        {
            /** fill is set by default to false
             * filloff command reset fill to false 
             * when fill is set to false the shapes drawn are not filled
            **/
           /** Rect rectangle = new Rect(color1, xPos, yPos, width, height);
            rectangle.draw(g);
            xPos += width;
            yPos += height;
           **/
             if (fill == false)
              {
                  g.DrawRectangle(Pen, xPos, yPos, width, height);
              }
              /**when fillon command is entered
               * fill is set to true
               * the next shapes drawn are filled until filloff is entered
               * */
            else
             {
                 g.FillRectangle(myBrush, xPos, yPos, width, height);
             }
             //update x and with bottom right corner of the rectangle
             xPos += width;
             yPos += height;
             
        }

        public void Triangle(int side1, int side2, int side3)
        {/**
            Triangle triangle = new Triangle(color1, xPos, yPos, side1,side2,side3);
            triangle.draw(g);
            
**/
            //in this case the triangle is equilateral

               Point AB = new Point((xPos + side1), yPos);
               Point AC = new Point(xPos, (yPos + side2));
               Point BC = new Point(xPos, yPos);
               Point[] point = new Point[] { AB, AC, BC };

               if (fill == false)
               {
                   g.DrawPolygon(Pen, point);
               }
               else
               {
                   g.FillPolygon(myBrush, point);
               }
            //I had to update the position of the pen so i  picked point b 
            xPos += side1;


        }

        public void Circle(int radius)
        {/**
            Circle circle = new Circle(color1, xPos, yPos, radius);
            circle.draw(g);
           

            **/
            int toX = 2 * radius;
               int toY = 2 * radius;
               if (fill == false)
               {
                   
                     g.DrawEllipse(Pen, xPos, yPos, toX, toY);
               }
               else
               {
                   g.FillEllipse(myBrush, xPos, yPos, toX, toY);
               }
            //update x and y position of pen into the centre of the circle
            xPos += radius;
            yPos += radius;
        }

        public void clear()
        {
            g.Clear(Color.White);

        }

        public void reset()
        {
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
            myBrush = new SolidBrush(Color.Black);

        }

        public void fillOn()
        {
            //when fillon command entered, fill set to true
            this.fill = true;
        }

        public void fillOff()
        {
            //when fillon command entered, fill set to true
            this.fill = false;
        }

        public void program_command(string line)
        {
            try
            {
                splitter = line.Trim().Split(' ');

            }
            catch (System.IndexOutOfRangeException f)
            {
                System.Windows.Forms.MessageBox.Show("The command is empty!");
            }
            //split line by space and check if the first index of the splitter is an eligible command
            if (splitter[0].Equals("drawto"))
            {

                if (splitter.Length > 3)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                else if (splitter.Length < 3)
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing parameters!");
                }
                else
                {

                    try
                    {
                        int X = Int32.Parse(splitter[1]);
                        int Y = Int32.Parse(splitter[2]);
                        drawTo(X, Y);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                    }


                }
            }


            else if (splitter[0].Equals("pen"))

            {

                //when command is too long ... pen red 2 anything
                if (splitter.Length > 3)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                if (splitter.Length < 3)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!Missing parameters");
                }
                else if (splitter[1].Equals("red"))
                {

                    try
                    {

                        thickness = Int32.Parse(splitter[2]);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                    }
                    pen(Color.Red, thickness);
                    myBrush = new SolidBrush(Color.Red);

                }



                else if (splitter[1].Equals("blue"))
                {

                    try
                    {
                        thickness = Int32.Parse(splitter[2]);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                    }
                    pen(Color.Blue, thickness);
                    myBrush = new SolidBrush(Color.Blue);


                }

                else if (splitter[1].Equals("yellow"))
                {

                    try
                    {
                        int thickness = Int32.Parse(splitter[2]);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                    }
                    pen(Color.Yellow, thickness);
                    myBrush = new SolidBrush(Color.Yellow);


                }

                else if (splitter[1].Equals("green"))
                {

                    try
                    {
                        int thickness = Int32.Parse(splitter[2]);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter numbers after command");
                    }
                    // when user change color of the pen, a new brush is created with same color

                    pen(Color.Green, thickness);
                    myBrush = new SolidBrush(Color.Green);

                }

                else
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an undefined color! Only blue,yellow,red,green colors are allowed");
                }

            }

            else if (splitter[0].Equals("moveto"))
            {
                if (splitter.Length > 3)
                {
                    ////when command moveto doesn't have parameters
                    System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Please enter 2 parameters  after moveto command!");
                }
                else if (splitter.Length > 3)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                else
                {

                    try
                    {
                        int X = Int32.Parse(splitter[1]);
                        int Y = Int32.Parse(splitter[2]);
                        moveTo(X, Y);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter only numbers after moveto command!");
                    }


                }
            }


            else if (splitter[0].Equals("rectangle"))
            {
                if (splitter.Length > 3)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                if (splitter.Length < 3)
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing width and height parameters!");
                }
                else
                {

                    try
                    {
                        int width = Int32.Parse(splitter[1]);
                        int height = Int32.Parse(splitter[2]);
                        Rectangle(width, height);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter 2 numbers after draw rectangle command");
                    }


                }
            }
            else if (splitter[0].Equals("triangle"))
            {/// when user type triangle 30 30 30 40
                if (splitter.Length > 4)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                //when user forget to type a complete command.
                // less than 3 sides
                else if (splitter.Length < 4)
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Please enter 3 parameters after draw triangle command!");
                }
                else
                {
                    try
                    {
                        int side1 = Int32.Parse(splitter[1]);
                        int side2 = Int32.Parse(splitter[2]);
                        int side3 = Int32.Parse(splitter[3]);
                        Triangle(side1, side2, side3);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("the parameters are in a wrong format! Please enter side1, side2, and side3 values after draw triangle command!");
                    }

                }
            }
            else if (splitter[0].Equals("circle"))
            {
                if (splitter.Length > 2)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                //when user enter only circle whithout any radius
                else if (splitter.Length < 2)
                {
                    System.Windows.Forms.MessageBox.Show("You have entered an incomplete  command! Missing radius!");
                }
                else
                {

                    try
                    {
                        int radius = Int32.Parse(splitter[1]);
                        Circle(radius);
                    }
                    catch (System.FormatException f)
                    {
                        System.Windows.Forms.MessageBox.Show("The parameters are in a wrong format! Please enter a number after draw circle command ");
                    }

                }
            }




            else if (splitter[0].Equals("clear"))
            {
                if (splitter.Length > 1)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                else
                {
                    clear();
                }
            }

            else if (splitter[0].Equals("reset"))
            {//the canvas is cleared, the pen keep same position, color and thickness
                if (splitter.Length > 1)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                else
                {
                    reset();
                }

            }
            else if (splitter[0].Equals("fillon"))
            {
                if (splitter.Length > 1)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                else
                {
                    /**set the function fillon to true
                    //any shape drawn from now will pe filled by the brush
                    the brush will have the same color like the pen
                    always when user change the color of the pen, a new brush with
                    the same color is created and waits for the command fillon
                    all the shapes will be filled until fillof command
                    **/
                    fillOn();
                }
            }
            else if (splitter[0].Equals("filloff"))
            {
                if (splitter.Length > 1)
                {
                    System.Windows.Forms.MessageBox.Show("you have entered an invalid command!To many parameters");
                }
                else
                {
                    //the command disable the brush 
                    fillOff();
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("You have entered an wrong command! Please try again!");
            }
            //reset commandLine
            //canvas.programArea.Text = " ";
            //update the picturebox



        }

    }
}
