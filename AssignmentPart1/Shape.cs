﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentPart1
{
   abstract class Shape
    {
        protected int x, y;
        protected Color color;

        public Shape(Color color, int x, int y)
        {
            
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public abstract void draw(Graphics g);

    }
}
