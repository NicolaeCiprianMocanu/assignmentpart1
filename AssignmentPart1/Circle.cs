﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForms;

namespace AssignmentPart1
{
    class Circle : Shape
    {
        int radius;
        
        private static Canvas canvas = new Canvas();

        public Circle(Color color, int x, int y, int radius) : base(color, x, y)
        {
            this.radius = radius;

        }

        public override void draw(Graphics g)
        {
            
            if (canvas.fill == false)
            {
                g.DrawEllipse(canvas.Pen, x, y, 2 * radius, 2 * radius);
            }
            else
            {
                g.FillRectangle(canvas.myBrush, x, y, 2 * radius, 2 * radius);
            }
         
        }
    }
}
