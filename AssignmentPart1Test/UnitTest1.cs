using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using WindowsForms;
using System.Drawing;
using AssignmentPart1;
using System.IO;

namespace AssignmentPart1Tests
{
    [TestClass]
    public class CanvasTest
    {
        //<summary>
        //test a valid moveto command
        //checks if expected xPos and yPos to be same like in user command string
        //</summary>
        [TestMethod]
        public void TestMoveToValid()
        {
            try
            {
                Canvas canvas = new Canvas();

                String user_command = "moveto 100 100";
                String[] splitter = user_command.Split(' ');
                int x = canvas.xPos;
                int y = canvas.yPos;
                int toX = Int32.Parse(splitter[1]);
                int toY = Int32.Parse(splitter[2]);

                canvas.moveTo(toX, toY);


                Assert.AreEqual(toX, x, 0.001, "wrong x");
                Assert.AreEqual(toY, y, 0.001, "wrong y");
            }
            catch (System.IO.FileNotFoundException E)
            {
                Console.WriteLine(E.FileName);
            }

        }

        //<summary>
        //test a invalid moveto command
        //</summary>
        [TestMethod]
        public void TestMoveToInvalid()
        {
            String user_command;
            String[] splitter;
            int x, y, toX, toY;
            String s = "";
            try
            {
                Canvas canvas = new Canvas();

                user_command = "moveto 100";
                splitter = user_command.Split(' ');
                x = canvas.xPos;
                y = canvas.yPos;
                toX = Int32.Parse(splitter[1]);

                toY = Int32.Parse(splitter[2]);
                try
                {
                    canvas.moveTo(toX, toY);
                }
                catch (ApplicationException e)
                {
                    s = e.Message;
                }

                Assert.AreEqual("\n You have entered an incomplete  command! Please enter 2 parameters  after moveto command!", s);

            }
            catch (System.IO.FileNotFoundException E)
            {
                Console.WriteLine(E.FileName);
            }
        }
        //<summary>
        //test a invalid  command
        //</summary>
        [TestMethod]
        public void TestInvalidCommand()
        {
            String user_command;
            String[] splitter;
            int x, y, toX, toY;
            String s = "";
            try
            {
                Canvas canvas = new Canvas();

                user_command = "xxx 100";
                splitter = user_command.Split(' ');
                x = canvas.xPos;
                y = canvas.yPos;
                toX = Int32.Parse(splitter[1]);

                toY = Int32.Parse(splitter[2]);
                try
                {
                    canvas.moveTo(toX, toY);
                }
                catch (ApplicationException e)
                {
                    s = e.Message;
                }

                Assert.AreEqual("\n Unknown command ", s);

            }
            catch (System.IO.FileNotFoundException E)
            {
                Console.WriteLine(E.FileName);
            }
        }

        [TestMethod]
        public void TestValidExpresionCountVariable()
        {
            try
            {
                String user_command = "count += 10";
                String[] splitter = user_command.Split(' ');
                
                
                int count_test = Int32.Parse(splitter[2]); 
                int count = 5;
                count += count_test;
                Assert.AreEqual(count, 15, 0.001, "wrong incrementation");
            }catch(FileNotFoundException f)
            {

            }
        }
        [TestMethod]
        public void TestInvalidExpresionCountVariable()
        {
            
            String s="";
            int count = 5;
            String user_command = "count += 5n";
            String[] splitter = user_command.Split(' ');
            try
            {
                int count_test = Int32.Parse(splitter[2]);
                count += count_test;
            }
            
              catch (FormatException e)
            {
                s = "Please Enter a valid number";
            }
            Assert.AreEqual("Please Enter a valid number", s);

        }

        [TestMethod]
        public void TestInstantiationVariableWidth()
        {

            String s = "";
            
            String user_command = "width += 2 2";
            String[] splitter = user_command.Split(' ');
            if(user_command.Length != 3)
            {
                s = "you entered too many parameters";
            }

            Assert.AreEqual("you entered too many parameters", s);

        }

        [TestMethod]
        public void TestValidCubeCommand()
        {

            String s="";

            String user_command = "cube 22 22 22";
            String[] splitter = user_command.Split(' ');
            if (user_command.Length != 4)
            {
                s = "The cube command is fine";
            }

            Assert.AreEqual("The cube command is fine", s);

        }

        [TestMethod]
        public void TestInvalidCubeCommand()
        {

            String s = "";
            try
            {
                String user_command = "cube 22 22 22n";
                String[] splitter = user_command.Split(' ');
                int length = Int32.Parse(splitter[3]);
            }catch(FormatException e)
            
            {
                s = "the last parameter is not an integer";
            }

            Assert.AreEqual("the last parameter is not an integer", s);

        }
    }
}